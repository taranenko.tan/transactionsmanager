﻿using CQRS.Commands;
using CQRS.Commands.Transactions;
using CQRS.Handlers;
using CQRS.Handlers.Transactions;
using CQRS.Models;
using CQRS.Quiries;
using DAL;
using Microsoft.Extensions.Logging;
using Moq;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace TransactionsManager.Tests
{
    public class TransactionsHandlersShould
    {
        [Fact]
        public async Task GetTransactionById()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange               
                Mock<ILogger<GetTransactionByIdHandler>> mockLogger = new Mock<ILogger<GetTransactionByIdHandler>>();
                var quey = new GetTransactionByIdQuery(2);

                var handler = new GetTransactionByIdHandler(context, mockLogger.Object);

                //Act
                var sut = await handler.Handle(quey, CancellationToken.None);

                //Assert
                Assert.NotNull(sut);
                Assert.Equal(2, sut.TransactionId);
            }
        }

        [Fact]
        public async Task GetAllTransactions()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange
                Mock<ILogger<GetTransactionAPIListHandler>> mockLogger = new Mock<ILogger<GetTransactionAPIListHandler>>();
                var query = new GetTransactionAPIListQuery();

                var handler = new GetTransactionAPIListHandler(context, mockLogger.Object);

                //Act
                var sut = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(3, sut.Count);
            }
        }

        [Fact]
        public async Task GetAllTransactionsByType()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange
                Mock<ILogger<GetTransactionAPIListHandler>> mockLogger = new Mock<ILogger<GetTransactionAPIListHandler>>();
                var typeFilter = "Refill";
                var query = new GetTransactionAPIListQuery(type: typeFilter);

                var handler = new GetTransactionAPIListHandler(context, mockLogger.Object);

                //Act
                var sut = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Equal(2, sut.Count);
                Assert.Equal(typeFilter, sut[0].Type);
            }
        }

        [Fact]
        public async Task GetAllTransactionsByStatus()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange
                Mock<ILogger<GetTransactionAPIListHandler>> mockLogger = new Mock<ILogger<GetTransactionAPIListHandler>>();
                var statusFilter = "Completed";
                var query = new GetTransactionAPIListQuery(status: statusFilter);

                var handler = new GetTransactionAPIListHandler(context, mockLogger.Object);

                //Act
                var sut = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Single(sut);
                Assert.Equal(statusFilter, sut[0].Status);
            }
        }

        [Fact]
        public async Task GetAllTransactionsByClient()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange
                Mock<ILogger<GetTransactionAPIListHandler>> mockLogger = new Mock<ILogger<GetTransactionAPIListHandler>>();
                var clientFilter = "Caldwell Reid";
                var query = new GetTransactionAPIListQuery(clientName: clientFilter);

                var handler = new GetTransactionAPIListHandler(context, mockLogger.Object);

                //Act
                var sut = await handler.Handle(query, CancellationToken.None);

                //Assert
                Assert.Single(sut);
                Assert.Equal(clientFilter, sut[0].ClientName);
            }
        }

        [Fact]
        public async Task DeleteTransactionById()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange
                Mock<ILogger<DeleteTransactionHandler>> mockLoggerDelete = new Mock<ILogger<DeleteTransactionHandler>>();

                var id = 1;
                var command = new DeleteTransactionCommand(id);

                var handler = new DeleteTransactionHandler(context, mockLoggerDelete.Object);

                //Act
                await handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.Equal(2, context.Transactions.Count());
                Assert.Null(context.Transactions.FirstOrDefault(t => t.Id == id));
            }
        }

        [Fact]
        public async Task InserTransaction()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange
                Mock<ILogger<InsertTransactionHandler>> mockLogger = new Mock<ILogger<InsertTransactionHandler>>();
                 
                var transaction = new TransactionAPI
                {
                    Status = "Completed",
                    Type = "Refill",
                    ClientName = "Quentin Bonner",
                    Amount = "64.52"
                };
                var command = new InsertTransactionCommand(transaction);

                var handler = new InsertTransactionHandler(context, mockLogger.Object);

                //Act
                await handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.Equal(4, context.Transactions.Count());
                Assert.Equal("Quentin Bonner", context.Transactions.LastOrDefault().Client);
            }


        }

        [Fact]
        public async Task UpdateTransaction()
        {
            using (var context = new TransactionContext(UnitTestHelper.GetUnitTestDbOption()))
            {
                //Arrange
                Mock<ILogger<PutTransactionHandler>> mockLogger = new Mock<ILogger<PutTransactionHandler>>();

                var id = 1;
                var status = "Completed";
                var command = new PutTransactionCommand(id, status);

                var handler = new PutTransactionHandler(context, mockLogger.Object);

                //Act
                await handler.Handle(command, CancellationToken.None);

                //Assert
                Assert.Equal(status, context.Transactions.FirstOrDefault().Status);
            }
        }
    }
}
