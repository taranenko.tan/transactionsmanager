﻿using DAL;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace TransactionsManager.Tests
{
    public class UnitTestHelper
    {
        public static DbContextOptions<TransactionContext> GetUnitTestDbOption()
        {
            var options = new DbContextOptionsBuilder<TransactionContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new TransactionContext(options))
            {
                SeedData(context);
            }

            return options;
        }

        private static void SeedData(TransactionContext context)
        {
            Transaction[] transactions =
            {
                new Transaction{Id = 1, Status="Pending", Type = "Withdrawal", Client = "Dale Cotton", Amount=28.43m},
                new Transaction{Id = 2, Status="Completed", Type = "Refill", Client = "Paul Carter", Amount=45.16m},
                new Transaction{Id = 3, Status="Cancelled", Type = "Refill", Client = "Caldwell Reid", Amount=63.00m}
            };

            context.Transactions.AddRange(transactions);
            context.SaveChanges();
        }
    }
}
