﻿using CQRS.Models;
using CQRS.Quiries;
using DAL;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class GetTransactionByIdHandler : IRequestHandler<GetTransactionByIdQuery, TransactionAPI>
    {
        private readonly TransactionContext _context;
        private readonly ILogger<GetTransactionByIdHandler> _logger;

        public GetTransactionByIdHandler(TransactionContext context, ILogger<GetTransactionByIdHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        public Task<TransactionAPI> Handle(GetTransactionByIdQuery request, CancellationToken cancellationToken)
        {
            TransactionAPI transactionAPI = null;
            try
            {
                var transaction = _context.Transactions.Find(request.Id);
                transactionAPI = transaction != null ? Helpers.MyMapper.MapToTransactionAPI(transaction) : null;
            }
            catch (InvalidOperationException e)
            {
                _logger.LogError(e.Message);
                return Task.FromResult<TransactionAPI>(null);
            }
            return Task.FromResult(transactionAPI);
        }
    }
}
