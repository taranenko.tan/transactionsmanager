﻿using CQRS.Commands;
using DAL;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class DeleteTransactionHandler : IRequestHandler<DeleteTransactionCommand, int>
    {
        private readonly TransactionContext _context;
        private readonly ILogger<DeleteTransactionHandler> _logger;

        public DeleteTransactionHandler(TransactionContext context, ILogger<DeleteTransactionHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        public Task<int> Handle(DeleteTransactionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var transaction = _context.Transactions.Find(request.Id);

                if (transaction != null)
                {
                    _context.Transactions.Remove(transaction);
                    _context.SaveChanges();
                    return Task.FromResult(1);
                }
            }
            catch (InvalidOperationException e)
            {
                _logger.LogError(e.Message);
                return Task.FromResult(0);
            }
            return Task.FromResult(0);
        }
    }
}
