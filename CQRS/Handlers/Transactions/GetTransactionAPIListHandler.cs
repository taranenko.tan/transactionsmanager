﻿using CQRS.Helpers;
using CQRS.Models;
using CQRS.Quiries;
using DAL;
using DAL.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class GetTransactionAPIListHandler : IRequestHandler<GetTransactionAPIListQuery, List<TransactionAPI>>
    {
        private readonly TransactionContext _context;
        private readonly ILogger<GetTransactionAPIListHandler> _logger;

        public GetTransactionAPIListHandler(TransactionContext context, ILogger<GetTransactionAPIListHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        public Task<List<TransactionAPI>> Handle(GetTransactionAPIListQuery request, CancellationToken cancellationToken)
        {
            List<TransactionAPI> transactionsAPI = null;

            try
            {
                IQueryable<Transaction> transaction = _context.Transactions;

                if (!string.IsNullOrEmpty(request.Type))
                {
                    transaction = transaction.Where(t => t.Type.ToUpper().Contains(request.Type.ToUpper()));
                }

                if (!string.IsNullOrEmpty(request.Status))
                {
                    transaction = transaction.Where(t => t.Status.ToUpper().Contains(request.Status.ToUpper()));
                }

                if (!string.IsNullOrEmpty(request.ClientName))
                {
                    transaction = transaction.Where(t => t.Client.ToUpper().Contains(request.ClientName.ToUpper()));
                }

                transactionsAPI = transaction.Select(t => MyMapper.MapToTransactionAPI(t)).ToList();
            }
            catch (InvalidOperationException e)
            {
                _logger.LogError(e.Message);
                return Task.FromResult<List<TransactionAPI>>(null);
            }

            return Task.FromResult(transactionsAPI);
        }
    }
}
