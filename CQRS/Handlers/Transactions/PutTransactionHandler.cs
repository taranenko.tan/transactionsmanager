﻿using CQRS.Commands.Transactions;
using CQRS.Models;
using DAL;
using DAL.Entities;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers.Transactions
{
    public class PutTransactionHandler : IRequestHandler<PutTransactionCommand, TransactionAPI>
    {
        private readonly TransactionContext _context;
        private readonly ILogger<PutTransactionHandler> _logger;

        public PutTransactionHandler(TransactionContext context, ILogger<PutTransactionHandler> logger)
        {
            _context = context;
            _logger = logger;
        }

        public Task<TransactionAPI> Handle(PutTransactionCommand request, CancellationToken cancellationToken)
        {
            TransactionAPI transactionAPI = null;

            try
            {
                if (request.Status != null && request.Id > 0 && request.Status.Length > 2)
                {
                    var transaction = _context.Transactions.Find(request.Id);

                    if (transaction != null)
                    {
                        transaction.Status = request.Status;
                        _context.Transactions.Update(transaction);

                        _context.SaveChanges();

                        transactionAPI = Helpers.MyMapper.MapToTransactionAPI(transaction);
                    }
                }
            }
            catch (InvalidOperationException e)
            {
                _logger.LogDebug(e.Message);

                return Task.FromResult<TransactionAPI>(null);
            }

            return Task.FromResult(transactionAPI);
        }
    }
}
