﻿using CQRS.Commands;
using CQRS.Models;
using DAL;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers
{
    public class InsertTransactionHandler : IRequestHandler<InsertTransactionCommand, TransactionAPI>
    {
        private readonly TransactionContext _context;
        private readonly ILogger<InsertTransactionHandler> _logger;

        public InsertTransactionHandler(TransactionContext context, ILogger<InsertTransactionHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        public Task<TransactionAPI> Handle(InsertTransactionCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var transaction = Helpers.MyMapper.MapToTransaction(request.TransactionAPI);
                transaction.Id = 0;

                _context.Transactions.Add(transaction);
                _context.SaveChanges();

                request.TransactionAPI.TransactionId = transaction.Id;
            }
            catch (InvalidOperationException e)
            {
                _logger.LogDebug(e.Message);

                return Task.FromResult<TransactionAPI>(null);
            }
            catch(DbUpdateException e)
            {
                _logger.LogDebug(e.Message);

                return Task.FromResult<TransactionAPI>(null);
            }


            return Task.FromResult(request.TransactionAPI);
        }
    }
}
