﻿using CQRS.Quiries;
using CQRS.Quiries.ImportExportCSV;
using CsvHelper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers.ImportExportCVS
{
    public class ExportCSVHandler : IRequestHandler<ExportCSVQuery, string>
    {
        ILogger<ExportCSVHandler> _logger;
        IMediator _mediator;

        public ExportCSVHandler(ILogger<ExportCSVHandler> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        public async Task<string> Handle(ExportCSVQuery request, CancellationToken cancellationToken)
        {
            try
            {
                #region GetTransactions from DB

                var query = new GetTransactionAPIListQuery(request.Type, request.Status, string.Empty);
                var transactions = await _mediator.Send(query);

                if (transactions == null) return "Something went wrong";

                #endregion

                #region Export Transactions to file

                var path =
                    $"{Directory.GetCurrentDirectory()}\\FIles";

                using (var write = new StreamWriter($"{path}\\NewExport.csv"))
                using (var csv = new CsvWriter(write, CultureInfo.InvariantCulture))
                {
                    csv.WriteRecords(transactions);
                }

                #endregion
            }
            catch (Exception e)
            {
                _logger.LogDebug(e.Message);
                return "Something went wrong";
            }

            return "File was exported successfully";
        }
    }
}
