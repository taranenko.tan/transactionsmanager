﻿using CQRS.Commands;
using CQRS.Models;
using CsvHelper;
using DAL;
using DAL.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace CQRS.Handlers
{
    public class ImportCSVHandler : IRequestHandler<ImportCSVCommand, string>
    {
        private readonly TransactionContext _context;
        private readonly ILogger<ImportCSVHandler> _logger;

        public ImportCSVHandler(TransactionContext context, ILogger<ImportCSVHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        public Task<string> Handle(ImportCSVCommand request, CancellationToken cancellationToken)
        {
            List<TransactionAPI> transactionsAPI = new List<TransactionAPI>();

            try
            {
                #region ReadCSV

                var path = $"{Directory.GetCurrentDirectory()}\\FIles\\data.csv";

                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Read();
                    csv.ReadHeader();
                    while (csv.Read())
                    {
                        var transactionAPI = csv.GetRecord<TransactionAPI>();
                        transactionsAPI.Add(transactionAPI);
                    }
                }

                #endregion

                var transactions = transactionsAPI.Select(t => Helpers.MyMapper.MapToTransaction(t));

                #region Add Transactions to DB

                _context.Database.OpenConnection();

                var entityType = _context.Model.FindEntityType(typeof(Transaction));
                _context.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT {entityType.GetSchema()}.{entityType.GetTableName()} ON");

                _context.BulkMerge(transactions,
                    options =>
                    {
                        options.MergeKeepIdentity = true;
                        options.IgnoreOnMergeUpdateExpression = t => new { t.Type, t.Client, t.Amount };
                    });

                _context.Database.ExecuteSqlRaw($"SET IDENTITY_INSERT {entityType.GetSchema()}.{entityType.GetTableName()} OFF");

                #endregion

            }
            catch (FileNotFoundException e)
            {
                _logger.LogDebug(e.Message);
                return Task.FromResult("Something went wrong");
            }
            catch (InvalidOperationException e)
            {

                _logger.LogDebug(e.Message);
                return Task.FromResult("Something went wrong");
            }
            finally
            {
                _context.Database.CloseConnection();
            }

            return Task.FromResult("File was exported successfully");

        }
    }
}
