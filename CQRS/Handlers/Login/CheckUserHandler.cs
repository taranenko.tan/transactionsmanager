﻿using CQRS.Quiries.Login;
using DAL;
using DAL.Entities;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers.Login
{
    public class CheckUserHandler : IRequestHandler<CheckUserQuery, User>
    {
        private readonly ILogger<CheckUserHandler> _logger;
        private readonly TransactionContext _context;
        private readonly IPasswordHasher<User> _hasher;

        public CheckUserHandler(ILogger<CheckUserHandler> logger, TransactionContext context, IPasswordHasher<User> hasher)
        {
            _logger = logger;
            _context = context;
            _hasher = hasher;
        }

        public Task<User> Handle(CheckUserQuery request, CancellationToken cancellationToken)
        {
            User user = null;
            try
            {
                user = _context.Users.Where(u => u.Email == request.Login.Email).FirstOrDefault();
                if (user == null) return Task.FromResult<User>(null);

                var res = _hasher.VerifyHashedPassword(user, user.HashPassword, request.Login.Password);
                if (res == PasswordVerificationResult.Failed) return Task.FromResult<User>(null);

            }
            catch (InvalidOperationException e)
            {
                _logger.LogDebug(e.Message);
                return Task.FromResult<User>(null);
            }

            return Task.FromResult(user);
        }
    }
}
