﻿using CQRS.Commands.Login;
using CQRS.Models;
using CQRS.Quiries.Login;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS.Handlers.Login
{
    public class CreateTokenHandler : IRequestHandler<CreateTokenQuery, JWTModel>
    {
        private readonly ILogger<CreateTokenHandler> _logger;
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;

        public CreateTokenHandler(ILogger<CreateTokenHandler> logger, IMediator mediator, IConfiguration configuration)
        {
            _logger = logger;
            _mediator = mediator;
            _configuration = configuration;
        }

        public async Task<JWTModel> Handle(CreateTokenQuery request, CancellationToken cancellationToken)
        {
            JWTModel result = null;

            try
            {
                var query = new CheckUserQuery(request.Login);
                var user = await _mediator.Send(query);

                if (user != null)
                {
                    var claims = new []
                    {
                      new Claim(JwtRegisteredClaimNames.Sub, user.FirstName),
                      new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                      new Claim(JwtRegisteredClaimNames.UniqueName, user.FirstName)
                    };

                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Tokens:Key"]));

                    var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                    var token = new JwtSecurityToken(
                          _configuration["Tokens:Issuer"],
                          _configuration["Tokens:Audience"],
                          claims,
                          expires: DateTime.UtcNow.AddMinutes(10),
                          signingCredentials: creds);

                    result = new JWTModel
                    {
                        Token = new JwtSecurityTokenHandler().WriteToken(token),
                        Expiration = token.ValidTo
                    };
                }
            }
            catch (Exception e)
            {
                _logger.LogDebug(e.Message);
            }

            return result;
        }
    }
}
