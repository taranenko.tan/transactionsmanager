﻿using CQRS.Models;
using DAL.Entities;

namespace CQRS.Helpers
{
    public class MyMapper
    {
        public static TransactionAPI MapToTransactionAPI(Transaction transaction)
        {
            var transactionAPI = new TransactionAPI();

            transactionAPI.TransactionId = transaction.Id;
            transactionAPI.Status = transaction.Status;
            transactionAPI.Type = transaction.Type;
            transactionAPI.ClientName = transaction.Client;
            transactionAPI.Amount = $"${transaction.Amount}";

            return transactionAPI;
        }

        public static Transaction MapToTransaction(TransactionAPI transactionAPI)
        {
            var transaction = new Transaction();

            transaction.Id = transactionAPI.TransactionId;
            transaction.Status = transactionAPI.Status;
            transaction.Type = transactionAPI.Type;
            transaction.Client = transactionAPI.ClientName;
            transaction.Amount = ConvertStringToDecimal(transactionAPI.Amount);

            return transaction;
        }

        private static decimal ConvertStringToDecimal(string num)
        {
            decimal result = 0;

            if (!string.IsNullOrEmpty(num))
            {
                decimal.TryParse(num.Substring(1).Replace('.', ','), out result);
            }

            return result;
        }
    }
}
