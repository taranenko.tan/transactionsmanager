﻿using MediatR;

namespace CQRS.Quiries.ImportExportCSV
{
    public class ExportCSVQuery : IRequest<string>
    {
        public string Type { get; }
        public string Status { get; }
        public ExportCSVQuery(string type, string status)
        {
            Type = type;
            Status = status;
        }
    }
}
