﻿using CQRS.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Quiries
{
    public class GetTransactionByIdQuery : IRequest<TransactionAPI>
    {
        public int Id { get; set; }
        public GetTransactionByIdQuery(int id)
        {
            Id = id;
        }
    }
}
