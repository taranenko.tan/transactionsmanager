﻿using CQRS.Models;
using DAL.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Quiries
{
    public class GetTransactionAPIListQuery : IRequest<List<TransactionAPI>>
    {
        public string Type { get; }
        public string Status { get; }
        public string ClientName { get; }
        public GetTransactionAPIListQuery(string type = null, string status = null, string clientName = null)
        {
            Type = type;
            Status = status;
            ClientName = clientName;
        }
    }
}
