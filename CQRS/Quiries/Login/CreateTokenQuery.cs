﻿using CQRS.Models;
using MediatR;

namespace CQRS.Commands.Login
{
    public class CreateTokenQuery : IRequest<JWTModel>
    {
        public CreateTokenQuery(LoginModel login)
        {
            Login = login;
        }
        public LoginModel Login { get; set; }
    }
}
