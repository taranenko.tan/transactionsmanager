﻿using CQRS.Models;
using DAL.Entities;
using MediatR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Quiries.Login
{
    public class CheckUserQuery : IRequest<User>
    {
        public CheckUserQuery(LoginModel login)
        {
            Login = login;
        }

        public LoginModel Login { get; set; }
    }
}
