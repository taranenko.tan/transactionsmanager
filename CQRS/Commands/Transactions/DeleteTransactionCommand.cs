﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Commands
{
    public class DeleteTransactionCommand : IRequest<int>
    {
        public int Id { get; }
        public DeleteTransactionCommand(int id)
        {
            Id = id;
        }
    }
}
