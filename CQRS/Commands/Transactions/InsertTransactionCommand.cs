﻿using CQRS.Models;
using MediatR;

namespace CQRS.Commands
{
    public class InsertTransactionCommand : IRequest<TransactionAPI>
    {
        public TransactionAPI TransactionAPI { get; set; }
        public InsertTransactionCommand(TransactionAPI transactionAPI)
        {
            TransactionAPI = transactionAPI;
        }
    }
}
