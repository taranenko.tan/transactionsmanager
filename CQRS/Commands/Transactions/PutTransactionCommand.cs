﻿using CQRS.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Commands.Transactions
{
    public class PutTransactionCommand : IRequest<TransactionAPI>
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public PutTransactionCommand(int id, string status)
        {
            Id = id;
            Status = status;
        }
    }
}
