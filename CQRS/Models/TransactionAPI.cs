﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Models
{
    public class TransactionAPI
    {
        public int TransactionId { get; set; }
        [MinLength(3)]
        public string Status { get; set; }
        [MinLength(3)]
        public string Type { get; set; }
        [MinLength(3)]
        public string ClientName { get; set; }
        public string Amount { get; set; }
    }
}
