﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CQRS.Models
{
    public class JWTModel
    {
        public string Token { get; set; }
        public DateTime Expiration { get; set; }
    }
}
