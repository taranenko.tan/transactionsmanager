﻿using CQRS.Commands;
using CQRS.Handlers.ImportExportCVS;
using CQRS.Quiries;
using CQRS.Quiries.ImportExportCSV;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;


namespace TransactionsManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImportCSVController : ControllerBase
    {
        private readonly IMediator _mediatR;

        public ImportCSVController(IMediator mediatR)
        {
            _mediatR = mediatR;
        }

        [HttpGet]
        public async Task<ActionResult<int>> Get()
        {
            var command = new ImportCSVCommand();
            var result = await _mediatR.Send(command);

            return result != null ? (ActionResult)Ok() : NotFound();
        }
    }
}
