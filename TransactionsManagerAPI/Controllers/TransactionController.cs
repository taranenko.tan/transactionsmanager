﻿using CQRS.Commands;
using CQRS.Commands.Transactions;
using CQRS.Models;
using CQRS.Quiries;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace TransactionsManagerAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TransactionController : ControllerBase
    {
        private readonly IMediator _mediatR;

        public TransactionController(IMediator mediator)
        {
            _mediatR = mediator;
        }

        // GET: api/<TransactionController>/
        [HttpGet]
        public async Task<ActionResult<List<TransactionAPI>>> Get(string type, string status, string clientName)
        {
            var query = new GetTransactionAPIListQuery(type, status, clientName);
            var restult = await _mediatR.Send(query);

            return restult != null ? (ActionResult)Ok(restult) : NotFound();
        }

        // GET api/<TransactionController>/
        [HttpGet("{id}")]
        public async Task<ActionResult<TransactionAPI>> Get(int id)
        {
            var query = new GetTransactionByIdQuery(id);
            var result = await _mediatR.Send(query);

            return result != null ? (ActionResult)Ok(result) : NotFound();
        }

        // POST api/<TransactionController>/
        [HttpPost]
        public async Task<ActionResult<TransactionAPI>> Post(TransactionAPI transaction)
        {
            var command = new InsertTransactionCommand(transaction);
            var result = await _mediatR.Send(command);

            return result != null ? (ActionResult)Ok(result) : NotFound();
        }

        // PUT api/<TransactionController>/
        [HttpPut("{id}")]
        public async Task<ActionResult<TransactionAPI>> Put(int id, string status)
        {
            var command = new PutTransactionCommand(id, status);
            var result = await _mediatR.Send(command);

            return result != null ? (ActionResult)Ok(result) : NotFound();
        }

        // DELETE api/<TransactionController>/
        [HttpDelete("{id}")]
        public async Task<ActionResult<int>> Delete(int id)
        {
            var command = new DeleteTransactionCommand(id);
            var result = await _mediatR.Send(command);

            return result != 0 ? (ActionResult)Ok() : NotFound();
        }
    }
}
