﻿using CQRS.Commands.Login;
using CQRS.Models;
using DAL;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionsManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IMediator _mediatR;

        public AuthenticationController(IMediator mediatR)
        {
            _mediatR = mediatR;
        }

        [HttpPost]
        public async Task<ActionResult<JWTModel>> Post(LoginModel login)
        {
            var query = new CreateTokenQuery(login);
            var res = await _mediatR.Send(query);

            return res != null ? (ActionResult)Created("",res) : NotFound();
        }
    }
}
