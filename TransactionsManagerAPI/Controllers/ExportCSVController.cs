﻿using CQRS.Quiries.ImportExportCSV;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransactionsManagerAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExportCSVController : ControllerBase
    {
        private readonly IMediator _mediatR;

        public ExportCSVController(IMediator mediatR)
        {
            _mediatR = mediatR;
        }

        [HttpGet]
        public async Task<ActionResult<int>> Get(string type, string status)
        {
            var query = new ExportCSVQuery(type, status);
            var result = await _mediatR.Send(query);

            return result != null ? (ActionResult)Ok(result) : NotFound();
        }
    }
}
