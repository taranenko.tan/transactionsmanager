﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
  
    public class Transaction : BaseClass
    {
        public string Status { get; set; }
        public string Type { get; set; }
        public string Client { get; set; }
        public decimal Amount { get; set; }
    }
}
