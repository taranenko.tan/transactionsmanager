﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Person : BaseClass
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
