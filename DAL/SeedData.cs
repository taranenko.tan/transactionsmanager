﻿using DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace DAL
{
    public class SeedData
    {

        public static void SeedDataBase(TransactionContext context, IPasswordHasher<User> hasher)
        {
            context.Database.Migrate();

            if (!context.Users.Any())
            {
                var manager = new User
                {
                    FirstName = "Petro",
                    LastName = "Petrenko",
                    Email = "petrenko@gmail.com"
                };
                manager.HashPassword = hasher.HashPassword(manager, "123$Abc");

                context.Users.Add(manager);

            }

            context.SaveChanges();
        }
    }
}
