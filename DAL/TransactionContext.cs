﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class TransactionContext : DbContext
    {
        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=TransationsManager;MultipleActiveResultSets=true");
        //}

        public TransactionContext(DbContextOptions<TransactionContext> opt) : base(opt)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            ChangeTracker.LazyLoadingEnabled = false;
        }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<User> Users { get; set; }
    }
}
